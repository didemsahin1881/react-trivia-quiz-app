import React, { useState } from 'react'
import './Introduce.css';
import Dropdown from '../../components/dropdown/Dropdown'
import { useNavigate } from 'react-router-dom';

const Introduce = () => {
    const diffuculty = ["easy", "medium", "hard"];
    const [diffucultyChange, setDiffucultyChange] = useState("");
    console.log('diffucultyChange', diffucultyChange);
    const TOTAL_QUESTIONS = 10;
    const navigate = useNavigate();
    const startQuiz = () => {
        console.log('start quiz run');
        if (diffucultyChange) {
            navigate(`/quiz/${diffucultyChange}/${TOTAL_QUESTIONS}`);
        }
    }
  return (
    <div className='introduce'>
          <div className='introduce-container'>
              <img src="https://thetrainingarcade.com/wp-content/uploads/2020/06/Trivia-logo-01.png" alt=""></img>
              <Dropdown data={diffuculty} setDiffucultyChange = {setDiffucultyChange}></Dropdown>
              <div onClick={startQuiz} className='introduce-btn'>Quiz e Başla</div>
          </div>
    </div>
  )
}

export default Introduce
