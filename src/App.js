import { BrowserRouter, Routes, Route } from "react-router-dom";
import Introduce from "./pages/Introduce/Introduce";
import Quiz from "./pages/Quiz/Quiz";
function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Introduce />}></Route>
          <Route path="/quiz/:difficulty/:amount" element={<Quiz />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
