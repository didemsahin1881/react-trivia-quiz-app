import React from 'react'
import './Dropdown.css';
const Dropdown = ({data, setDiffucultyChange}) => {
  return (
    <div className='dropdown'>
          <select onChange={(e)=>setDiffucultyChange(e.target.value)} id="" name=''>
              {
                  data.map((dt, i) => (
                      <option value={dt} key={i} >{ dt}</option>
                  ))
              }
      </select>
    </div>
  )
}

export default Dropdown
